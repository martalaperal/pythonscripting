import maya.cmds as cmds
import random

WINDOW_ID = 'mlaperalscripts'

def increase_lights_intensity(percent_addition = 1.5):
    """
	This function looks for all Arnold lights in the scene
	that have the 'ai' prefix, then it will get their intensity
	and increase it by the percentage passed by argument.
	In case of no Arnold lights found, there will throw an error.
	"""       
    arnold_scene_lights = cmds.ls('ai*Light*')
    
    try:
        for ai_light in arnold_scene_lights:
            increased_intensity = cmds.getAttr('%s.intensity' % ai_light) * percent_addition
            cmds.setAttr('%s.intensity' % ai_light, increased_intensity)
    except:
        raise RuntimeError, 'No Arnold lights in the scene'
    else:
        print 'Arnold lights intensity succesfully increased %f' % percent_addition

def reference_robot_elements_count():
	"""
	This function gets all 'transform' type scene objects
	and then print in a formatted way, the list of robot
	components found among them.
	"""
	scene_objects = cmds.ls(type = 'transform')	        
	
	if not scene_objects:
		raise RuntimeError, 'There is no Robot objects to rename'
	
	found_objects = []
	
	for object_node in scene_objects:
	    
	    if str(object_node).find('Robot') >= 0:
	        found_objects.append(object_node)
	
	
	if len(found_objects) > 0:
	    print 'There have been found %i reference robot elements, these are the following with the prefix "RobotRig:":' % len(found_objects)		
	    for element in found_objects:
	        print '*', element.replace('RobotRig:','')

def create_random_spheres():

	"""
	This function create a random number of spheres
	with a random sphere closing angle.
	In case of success, it creates a group with them
	"""
	spheres_count = random.randrange(1,21)
	allowed_values = [0.5, 1, 2.5, 3]
	sphere_group = []
	
	for i in range(spheres_count):
		randomized_value = random.choice(allowed_values)	
		sphere_created = cmds.sphere(r = randomized_value, ssw = 0, esw = random.randint(10,360))[0]
		cmds.move(randomized_value,0,randomized_value*2, sphere_created)
		sphere_group.append(sphere_created)		
	
	group_name = 'spheresgroup_' + str(spheres_count)
	cmds.group(sphere_group, n = group_name)
	
	if len(sphere_group) > 0:
		print "There have been created %i spheres with random angle and grouped as '%s'" % (spheres_count, group_name)
	else:
		raise RuntimeError, 'An error occurred while creating a random number of spheres'

def delete_group_by_name(group_name):
	
	"""
	This functions obtain the group given
	by argument name, if not exists throws an error
	otherwise it deletes that group
	"""
	group_name = str(group_name)
	requested_group = cmds.ls(group_name)	        
	
	if requested_group:
		cmds.delete(requested_group)
		print "The '%s' group name has been correctly removed from scene" % group_name
	else:
		raise RuntimeError, "The '%s' group name does not match with the existing ones" % group_name

def close_window(*pArgs):	
	
	"""
	This function just close an existing 
	window previously created
	"""
	if cmds.window(WINDOW_ID, exists = True):
		cmds.deleteUI(WINDOW_ID)

def create_main_UI():
	
	"""
	This function creates an user interface
	where to choose between the different
	options of the functions listed before,
	introducing values in case of necessary
	"""
	if cmds.window(WINDOW_ID, exists = True):
		cmds.deleteUI(WINDOW_ID)

	cmds.window(WINDOW_ID, title = 'Testing scripts', sizeable = False, resizeToFitChildren = True)
	cmds.rowColumnLayout(numberOfColumns = 3, columnWidth = [(1, 200), (2, 50), (3, 50)], columnOffset = [(1, 'right', 5), (2, 'right' ,5), (3, 'right' ,5), (4, 'right' ,5)],  rowOffset = [(1, 'top', 5), (2, 'top', 5), (3, 'top', 5), (4, 'top', 5), (5, 'top', 5)], backgroundColor = [0.0, 0.5, 0.5])
	
	cmds.text(label = 'Increase Arnold lights intensity if any:')
	cmds.floatField('percent_increase')
	cmds.button(label = 'Increase', noBackground = True, command = lambda f: increase_lights_intensity(cmds.floatField('percent_increase', query = True, value = True)))
	
	cmds.text(label = 'Show reference robot elements:')
	cmds.separator(w = 5, style = 'none')
	cmds.button(label = 'Show', noBackground = True, command = lambda f: reference_robot_elements_count())
	
	cmds.text(label = 'Create random number of spheres:')
	cmds.separator(w = 5, style = 'none')
	cmds.button(label = 'Create', noBackground = True, command = lambda f: create_random_spheres())
	
	cmds.text(label = 'Remove group named:')
	cmds.textField('requested_group')
	cmds.button(label = 'Remove', noBackground = True, command = lambda f: delete_group_by_name(cmds.textField('requested_group', query = True, text = True)))
	
	cmds.separator(h = 15, style = 'none')
	cmds.button(label = 'Close', width = 100, backgroundColor = [1.0, 1.0, 0.0], command = close_window)
	
	cmds.showWindow()

create_main_UI()