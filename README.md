# README #

You can find in this directory some Python & Maya code in 'tests.py' file.

**User guide**:

To use the script, after open the scene called 'ArnoldRobot.mb' in Maya, you can do one of two:

* Open the Script Editor and load the script file, then execute the code in Python tab with play button.
* Save the script file in "C:\Users\[USER]\Documents\maya\scripts" and then in Python tab write 'import tests', then press play button.

In addition, after one of the previous execution, you can select from the console the entire code and drag it to the 'Custom' interface tab so it will create an automatic button with the functionality to launch it everytime you want :) !